CSE.30341.FA17: Project 04
==========================

This is the documentation for [Project 04] of [CSE.30341.FA17].

Members
-------

1. Domer McDomerson (dmcdomer@nd.edu)
2. Belle Fleur (bfleur@nd.edu)

Design
------

> 1. You will need to implement splitting of free blocks:
>
>   - When should you split a block?
>
>   - How should you split a block?

Response.

> 2. You will need to implement coalescing of free blocks:
>
>   - When should you coalescing block?
>
>   - How should you coalesce a block?

Response.

> 3. You will need to implement Next Fit.
>
>   - What information do you need to perform a Next Fit?
>
>   - How would you implement Next Fit?

Response.

> 4. You will need to implement Best Fit.
>
>   - What information do you need to perform a Best Fit?
>
>   - How would you implement Best Fit?

Response.

> 5. You will need to implement Worst Fit.
>
>   - What information do you need to perform a Worst Fit?
>
>   - How would you implement Worst Fit?

Response.

> 6. You will need to implement tracking of different information.
>
>   - What information will you want to track?
>
>   - How will you update these trackers?
>
>   - How will you report these trackers when the program ends?

Response.

Demonstration
-------------

> Place a link to your demonstration slides on [Google Drive].

Errata
------

> Describe any known errors, bugs, or deviations from the requirements.

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 04]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project04.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Drive]:     https://drive.google.com
